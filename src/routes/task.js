import express from 'express';
import TaskController from '../controllers/task.controller.js';
const router = new express.Router()

router.post('/tasks', TaskController.createTask)
router.get('/tasks', TaskController.getTasks)
router.get('/tasks/:id', TaskController.getTaskById)
router.patch('/tasks/:id', TaskController.updateTask)
router.delete('/tasks/:id', TaskController.deleteTask)

export default router;