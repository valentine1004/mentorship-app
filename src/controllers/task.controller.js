import Task from '../models/task.js';

export default class TaskController {
    static async createTask(req, res) {
        const task = new Task(req.body)

        try {
            await task.save()
            res.status(201).send(task)
        } catch (e) {
            res.status(400).send(e)
        }
    }

    static async getTasks(req, res) {
        try {
            const tasks = await Task.find({})
            res.send(tasks)
        } catch (e) {
            res.status(500).send()
        }
    }

    static async getTaskById(req, res) {
        const _id = req.params.id

        try {
            const task = await Task.findById(_id)

            if (!task) {
                return res.status(404).send()
            }

            res.send(task)
        } catch (e) {
            res.status(500).send()
        }
    }

    static async updateTask(req, res) {
        try {
            const task = await Task.findByIdAndUpdate(req.params.id, req.body, { new: true, runValidators: true })

            if (!task) {
                return res.status(404).send()
            }

            res.send(task)
        } catch (e) {
            res.status(400).send(e)
        }
    }

    static async deleteTask(req, res) {
        try {
            const task = await Task.findByIdAndDelete(req.params.id)

            if (!task) {
                res.status(404).send()
            }

            res.send(task)
        } catch (e) {
            res.status(500).send()
        }
    }
};